import React, { useState, useEffect, useRef } from 'react';
import flatpickr from 'flatpickr';
import { German } from 'flatpickr/dist/l10n/de';
import 'flatpickr/dist/flatpickr.min.css';
import '../customCss/flatpickr.theme.min.css';
import '../customCss/Form.css';
import '../customCss/fonts.css';

const Form = () => {
  const [startDate, setStartDate] = useState<Date | null>(null);
  const [endDate, setEndDate] = useState<Date | null>(null);
  const [adults, setAdults] = useState(0);
  const [showErrors, setShowErrors] = useState(false);
  const [children, setChildren] = useState([{ name: '', birthdate: '' }]);
  const [formError, setFormError] = useState(false);
  const dateInputRef = useRef<HTMLInputElement | null>(null);
  const fpInstance = useRef<flatpickr.Instance | null>(null);

  useEffect(() => {
    if (dateInputRef.current !== null) {
      const today = new Date();
      today.setHours(0, 0, 0, 0); // Setzt die Zeit auf den Start des Tages
      fpInstance.current = flatpickr(dateInputRef.current, {
        mode: 'range',
        locale: German,
        dateFormat: 'd.m.Y',
        showMonths: 2,
        onDayCreate: (dObj, dStr, fp, dayElem) => {
          // Markiert vergangene Daten
          if (dayElem.dateObj < today) {
            dayElem.classList.add('past-date');
          }
        },
        onChange: (selectedDates) => {
          setStartDate(selectedDates[0] || null);
          setEndDate(selectedDates[1] || null);
          if (selectedDates.length === 2) {
            dateInputRef.current!.value = `${selectedDates[0].toLocaleDateString()} - ${selectedDates[1].toLocaleDateString()}`;
            fpInstance.current?.close();
          }
        },
      });

      // Schließt den Datepicker, wenn User außerhalb klickt
      const handleClickOutside = (event: MouseEvent) => {
        if (dateInputRef.current && !dateInputRef.current.contains(event.target as Node)
            && fpInstance.current && fpInstance.current.calendarContainer
            && !fpInstance.current.calendarContainer.contains(event.target as Node)) {
          fpInstance.current?.close();
        }
      };

      document.addEventListener('mousedown', handleClickOutside);

      return () => {
        document.removeEventListener('mousedown', handleClickOutside);
        fpInstance.current?.destroy();
      };
    }
  }, []);

  // Funktion zum Hinzufügen eines Kindes zu der Liste
  const addChild = () => {
    setChildren([...children, { name: '', birthdate: '' }]);
  };

  // Funktion zum Entfernen eines Kindes aus der Liste
  const removeChild = (index: number) => {
    setChildren(children.filter((_, i) => i !== index));
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setShowErrors(true);

    const form = e.currentTarget;
    let hasError = false;

    // Überprüft die Eingabevalidität
    form.querySelectorAll<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>('input, select, textarea').forEach((input) => {
      if (!input.checkValidity()) {
        hasError = true;
        input.classList.add('invalid');
      } else {
        input.classList.remove('invalid');
      }
    });

    // Überprüft, ob das Start- und Enddatum ausgewählt ist
    if (!startDate || !endDate) {
      const dateButton = form.querySelector('input[name="date-range"]') as HTMLElement;
      if (dateButton) {
        hasError = true;
        dateButton.classList.add('invalid');
      }
    }

    // Validiert die Informationen der Kinder
    children.forEach((child, index) => {
      if (!child.name || !child.birthdate) {
        hasError = true;
        const nameInput = document.querySelectorAll('.form-input')[index * 2 + 5];
        const birthdateInputs = document.querySelectorAll('.form-geb-input');

        nameInput.classList.add('invalid');
        birthdateInputs[index * 3].classList.add('invalid');
        birthdateInputs[index * 3 + 1].classList.add('invalid');
        birthdateInputs[index * 3 + 2].classList.add('invalid');
      }
    });

    setFormError(hasError);
  };

  return (
    <div>
      <div className="form-container">
        <div className="content">
          <h1 className="heading">Ihr Wohlfühl-Urlaub im Almparadies Seitenalm!</h1>
          <p className='heading-text'>
            Bitte geben Sie uns Ihre <strong>Urlaubswünsche</strong> bekannt und wir werden uns umgehend um Ihren nächsten Urlaub kümmern.
          </p>
        </div>
      </div>
      <div className="form-backgound">
        <div className="form-align">
          <form onSubmit={handleSubmit} noValidate>
            <div className="form-content">
              <h2 className="form-heading">Ihre Urlaubsdaten</h2>
              <div className="form-field input-wrapper">
                <label>Reisezeitraum*</label>
                <input
                  name="date-range"
                  type="text"
                  className={`form-input ${showErrors && (!startDate || !endDate) ? 'invalid' : ''}`}
                  placeholder=""
                  readOnly
                  ref={dateInputRef}
                />
              </div>
              <div className="form-field">
                <label>Anzahl Erwachsene*</label>
                <input
                  className={`form-input ${showErrors && adults <= 0 ? 'invalid' : ''}`}
                  type="number"
                  name="adults"
                  value={adults}
                  min="1"
                  onChange={(e) => setAdults(parseInt(e.target.value))}
                  required
                />
              </div>
            </div>
            <div className="form-content">
              <h2 className="form-heading">Kinder</h2>
              <span className="form-span">
                Als Familienspezialist ist es uns wichtig, Ihnen ein maßgeschneidertes Angebot zu übermitteln. Bitte geben Sie uns daher den Vornamen und das
                Alter Ihrer Kinder/Ihres Kindes an.
              </span>
              {children.map((child, index) => (
                <div key={index}>
                  <div className="form-field">
                    <label>Name des Kindes*</label>
                    <input
                      className={`form-input ${showErrors && !child.name ? 'invalid' : ''}`}
                      type="text"
                      value={child.name}
                      onChange={(e) => {
                        const newChildren = [...children];
                        newChildren[index].name = e.target.value;
                        setChildren(newChildren);
                      }}
                      required
                    />
                  </div>
                  <div className="form-field">
                    <label>Geburtstag*</label>
                    <div className="form-input-group">
                      <select
                        className={`form-geb-input ${showErrors && !child.birthdate ? 'invalid' : ''}`}
                        value={child.birthdate.split('.')[0] || ''}
                        onChange={(e) => {
                          const newChildren = [...children];
                          newChildren[index].birthdate = `${e.target.value}.${newChildren[index].birthdate.split('.')[1] || ''}.${newChildren[index].birthdate.split('.')[2] || ''}`;
                          setChildren(newChildren);
                        }}
                        required>
                        <option value="" disabled>Tag</option>
                        {Array.from({ length: 31 }, (_, i) => (
                          <option key={i} value={`${i + 1 < 10 ? `0${i + 1}` : `${i + 1}`}`}>{i + 1}</option>
                        ))}
                      </select>
                      <select
                        className={`form-geb-input ${showErrors && !child.birthdate ? 'invalid' : ''}`}
                        value={child.birthdate.split('.')[1] || ''}
                        onChange={(e) => {
                          const newChildren = [...children];
                          newChildren[index].birthdate = `${newChildren[index].birthdate.split('.')[0] || ''}.${e.target.value}.${newChildren[index].birthdate.split('.')[2] || ''}`;
                          setChildren(newChildren);
                        }}
                        required>
                        <option value="" disabled>Monat</option>
                        {['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'].map((month, i) => (
                          <option key={i} value={month}>{month}</option>
                        ))}
                      </select>
                      <select
                        className={`form-geb-input ${showErrors && !child.birthdate ? 'invalid' : ''}`}
                        value={child.birthdate.split('.')[2] || ''}
                        onChange={(e) => {
                          const newChildren = [...children];
                          newChildren[index].birthdate = `${newChildren[index].birthdate.split('.')[0] || ''}.${newChildren[index].birthdate.split('.')[1] || ''}.${e.target.value}`;
                          setChildren(newChildren);
                        }}
                        required>
                        <option value="" disabled>Jahr</option>
                        {Array.from({ length: 100 }, (_, i) => (
                          <option key={i} value={`${new Date().getFullYear() - i}`}>{new Date().getFullYear() - i}</option>
                        ))}
                      </select>
                    </div>
                  </div>
                </div>
              ))}
              <div className="responsive-div">
                <p onClick={addChild} className="underlined add-child">Kind hinzufügen</p>
                <span className="arrow"></span>
                {children.length > 1 && (
                  <p onClick={() => removeChild(children.length - 1)} className="underlined remove-child button-delete">Kind entfernen</p>
                )}
              </div>
            </div>
            <div className="form-content">
              <h2 className="form-heading">Ihre Kontaktdaten</h2>
              <div className="form-field">
                <label>Vorname*</label>
                <input className={`form-input ${showErrors ? 'invalid' : ''}`} type="text" name="first-name" required />
              </div>
              <div className="form-field">
                <label>Nachname*</label>
                <input className={`form-input ${showErrors ? 'invalid' : ''}`} type="text" name="last-name" required />
              </div>
              <div className="form-field">
                <label>Geschlecht*</label>
                <select className={`form-input ${showErrors ? 'invalid' : ''}`} name="gender" required>
                  <option value="" selected disabled>Auswählen</option>
                  <option value="male">Männlich</option>
                  <option value="female">Weiblich</option>
                </select>
              </div>
              <div className="form-field">
                <label>E-Mail*</label>
                <input className={`form-input ${showErrors ? 'invalid' : ''}`} type="email" name="email" pattern="^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,}$" required />
              </div>
              <div className="form-field">
                <label>Land*</label>
                <select className={`form-input ${showErrors ? 'invalid' : ''}`} name="country" required>
                  <option value="" selected disabled>Auswählen</option>
                  <option value="DE">Deutschland</option><option value="AT">Österreich</option><option value="CH">Schweiz</option><option >-------------------</option><option value="AF">Afghanistan</option><option value="EG">Ägypten</option><option value="AX">Ålandinseln</option><option value="AL">Albanien</option><option value="DZ">Algerien</option><option value="AS">Amerikanisch-Samoa</option><option value="VI">Amerikanische Jungferninseln</option><option value="UM">Amerikanische Überseeinseln</option><option value="AD">Andorra</option><option value="AO">Angola</option><option value="AI">Anguilla</option><option value="AQ">Antarktis</option><option value="AG">Antigua und Barbuda</option><option value="GQ">Äquatorialguinea</option><option value="AR">Argentinien</option><option value="AM">Armenien</option><option value="AW">Aruba</option><option value="AZ">Aserbaidschan</option><option value="ET">Äthiopien</option><option value="AU">Australien</option><option value="BS">Bahamas</option><option value="BH">Bahrain</option><option value="BD">Bangladesch</option><option value="BB">Barbados</option><option value="BY">Belarus</option><option value="BE">Belgien</option><option value="BZ">Belize</option><option value="BJ">Benin</option><option value="BM">Bermuda</option><option value="BT">Bhutan</option><option value="BO">Bolivien</option><option value="BA">Bosnien und Herzegowina</option><option value="BW">Botsuana</option><option value="BV">Bouvetinsel</option><option value="BR">Brasilien</option><option value="VG">Britische Jungferninseln</option><option value="IO">Britisches Territorium im Indischen Ozean</option><option value="BN">Brunei Darussalam</option><option value="BG">Bulgarien</option><option value="BF">Burkina Faso</option><option value="BI">Burundi</option><option value="CV">Cabo Verde</option><option value="CL">Chile</option><option value="CN">China</option><option value="CK">Cookinseln</option><option value="CR">Costa Rica</option><option value="CI">Côte d’Ivoire</option><option value="CW">Curaçao</option><option value="DK">Dänemark</option><option value="DE">Deutschland</option><option value="DM">Dominica</option><option value="DO">Dominikanische Republik</option><option value="DJ">Dschibuti</option><option value="EC">Ecuador</option><option value="SV">El Salvador</option><option value="ER">Eritrea</option><option value="EE">Estland</option><option value="SZ">Eswatini</option><option value="FK">Falklandinseln</option><option value="FO">Färöer</option><option value="FJ">Fidschi</option><option value="FI">Finnland</option><option value="FR">Frankreich</option><option value="GF">Französisch-Guayana</option><option value="PF">Französisch-Polynesien</option><option value="TF">Französische Süd- und Antarktisgebiete</option><option value="GA">Gabun</option><option value="GM">Gambia</option><option value="GE">Georgien</option><option value="GH">Ghana</option><option value="GI">Gibraltar</option><option value="GD">Grenada</option><option value="GR">Griechenland</option><option value="GL">Grönland</option><option value="GP">Guadeloupe</option><option value="GU">Guam</option><option value="GT">Guatemala</option><option value="GG">Guernsey</option><option value="GN">Guinea</option><option value="GW">Guinea-Bissau</option><option value="GY">Guyana</option><option value="HT">Haiti</option><option value="HM">Heard und McDonaldinseln</option><option value="HN">Honduras</option><option value="IN">Indien</option><option value="ID">Indonesien</option><option value="IQ">Irak</option><option value="IR">Iran</option><option value="IE">Irland</option><option value="IS">Island</option><option value="IM">Isle of Man</option><option value="IL">Israel</option><option value="IT">Italien</option><option value="JM">Jamaika</option><option value="JP">Japan</option><option value="YE">Jemen</option><option value="JE">Jersey</option><option value="JO">Jordanien</option><option value="KY">Kaimaninseln</option><option value="KH">Kambodscha</option><option value="CM">Kamerun</option><option value="CA">Kanada</option><option value="BQ">Karibische Niederlande</option><option value="KZ">Kasachstan</option><option value="QA">Katar</option><option value="KE">Kenia</option><option value="KG">Kirgisistan</option><option value="KI">Kiribati</option><option value="CC">Kokosinseln</option><option value="CO">Kolumbien</option><option value="KM">Komoren</option><option value="CG">Kongo-Brazzaville</option><option value="CD">Kongo-Kinshasa</option><option value="HR">Kroatien</option><option value="CU">Kuba</option><option value="KW">Kuwait</option><option value="LA">Laos</option><option value="LS">Lesotho</option><option value="LV">Lettland</option><option value="LB">Libanon</option><option value="LR">Liberia</option><option value="LY">Libyen</option><option value="LI">Liechtenstein</option><option value="LT">Litauen</option><option value="LU">Luxemburg</option><option value="MG">Madagaskar</option><option value="MW">Malawi</option><option value="MY">Malaysia</option><option value="MV">Malediven</option><option value="ML">Mali</option><option value="MT">Malta</option><option value="MA">Marokko</option><option value="MH">Marshallinseln</option><option value="MQ">Martinique</option><option value="MR">Mauretanien</option><option value="MU">Mauritius</option><option value="YT">Mayotte</option><option value="MX">Mexiko</option><option value="FM">Mikronesien</option><option value="MC">Monaco</option><option value="MN">Mongolei</option><option value="ME">Montenegro</option><option value="MS">Montserrat</option><option value="MZ">Mosambik</option><option value="MM">Myanmar</option><option value="NA">Namibia</option><option value="NR">Nauru</option><option value="NP">Nepal</option><option value="NC">Neukaledonien</option><option value="NZ">Neuseeland</option><option value="NI">Nicaragua</option><option value="NL">Niederlande</option><option value="NE">Niger</option><option value="NG">Nigeria</option><option value="NU">Niue</option><option value="KP">Nordkorea</option><option value="MP">Nördliche Marianen</option><option value="MK">Nordmazedonien</option><option value="NF">Norfolkinsel</option><option value="NO">Norwegen</option><option value="OM">Oman</option><option value="AT">Österreich</option><option value="PK">Pakistan</option><option value="PS">Palästinensische Autonomiegebiete</option><option value="PW">Palau</option><option value="PA">Panama</option><option value="PG">Papua-Neuguinea</option><option value="PY">Paraguay</option><option value="PE">Peru</option><option value="PH">Philippinen</option><option value="PN">Pitcairninseln</option><option value="PL">Polen</option><option value="PT">Portugal</option><option value="PR">Puerto Rico</option><option value="MD">Republik Moldau</option><option value="RE">Réunion</option><option value="RW">Ruanda</option><option value="RO">Rumänien</option><option value="RU">Russland</option><option value="SB">Salomonen</option><option value="ZM">Sambia</option><option value="WS">Samoa</option><option value="SM">San Marino</option><option value="ST">São Tomé und Príncipe</option><option value="SA">Saudi-Arabien</option><option value="SE">Schweden</option><option value="CH">Schweiz</option><option value="SN">Senegal</option><option value="RS">Serbien</option><option value="SC">Seychellen</option><option value="SL">Sierra Leone</option><option value="ZW">Simbabwe</option><option value="SG">Singapur</option><option value="SX">Sint Maarten</option><option value="SK">Slowakei</option><option value="SI">Slowenien</option><option value="SO">Somalia</option><option value="HK">Sonderverwaltungsregion Hongkong</option><option value="MO">Sonderverwaltungsregion Macau</option><option value="ES">Spanien</option><option value="SJ">Spitzbergen und Jan Mayen</option><option value="LK">Sri Lanka</option><option value="BL">St. Barthélemy</option><option value="SH">St. Helena</option><option value="KN">St. Kitts und Nevis</option><option value="LC">St. Lucia</option><option value="MF">St. Martin</option><option value="PM">St. Pierre und Miquelon</option><option value="VC">St. Vincent und die Grenadinen</option><option value="ZA">Südafrika</option><option value="SD">Sudan</option><option value="GS">Südgeorgien und die Südlichen Sandwichinseln</option><option value="KR">Südkorea</option><option value="SS">Südsudan</option><option value="SR">Suriname</option><option value="SY">Syrien</option><option value="TJ">Tadschikistan</option><option value="TW">Taiwan</option><option value="TZ">Tansania</option><option value="TH">Thailand</option><option value="TL">Timor-Leste</option><option value="TG">Togo</option><option value="TK">Tokelau</option><option value="TO">Tonga</option><option value="TT">Trinidad und Tobago</option><option value="TD">Tschad</option><option value="CZ">Tschechien</option><option value="TN">Tunesien</option><option value="TR">Türkei</option><option value="TM">Turkmenistan</option><option value="TC">Turks- und Caicosinseln</option><option value="TV">Tuvalu</option><option value="UG">Uganda</option><option value="UA">Ukraine</option><option value="HU">Ungarn</option><option value="UY">Uruguay</option><option value="UZ">Usbekistan</option><option value="VU">Vanuatu</option><option value="VA">Vatikanstadt</option><option value="VE">Venezuela</option><option value="AE">Vereinigte Arabische Emirate</option><option value="US">Vereinigte Staaten</option><option value="GB">Vereinigtes Königreich</option><option value="VN">Vietnam</option><option value="WF">Wallis und Futuna</option><option value="CX">Weihnachtsinsel</option><option value="EH">Westsahara</option><option value="CF">Zentralafrikanische Republik</option><option value="CY">Zypern</option>
                </select>
              </div>
              <div className="form-field">
                <label>PLZ*</label>
                <input className={`form-input ${showErrors ? 'invalid' : ''}`} type="text" name="zipcode" required />
              </div>
              <div className="form-field">
                <label>Stadt*</label>
                <input className={`form-input ${showErrors ? 'invalid' : ''}`} type="text" name="city" required />
              </div>
              <div className="form-field">
                <label>Straße*</label>
                <input className={`form-input ${showErrors ? 'invalid' : ''}`} type="text" name="street" required />
              </div>
              <div className="form-field">
                <label>Telefon</label>
                <input className="form-input" type="tel" name="phone" />
              </div>
              <div className="form-field">
                <label>Fragen oder Wünsche</label>
                <textarea className="form-input" name="comments" rows={4}></textarea>
              </div>
            </div>
            {formError && (
              <p className="error-message">Beim Senden des Formulars ist ein Fehler aufgetreten! Die ungültigen Felder wurden hervorgehoben.</p>
            )}
            <div className="form-field">
              <button type="submit" className="submit-button">Anfrage Absenden</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Form;