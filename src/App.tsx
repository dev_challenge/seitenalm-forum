import React from 'react';
import Header from './components/Header';
import Form from './components/Form';
import './App.css';
import './customCss/fonts.css';

const App = () => {
  return (
    <div className="app-container">
      <Header />
      <Form />
    </div>
  );
};
export default App;